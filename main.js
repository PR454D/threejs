import './style.css'

import * as THREE from 'three';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
const scene = new THREE.Scene();


const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );


const renderer = new THREE.WebGLRenderer({
  canvas: document.querySelector('#bg'),
});


renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );
camera.position.setZ(30);

renderer.render( scene, camera );


const geometry = new THREE.TorusGeometry( 8, 3, 6, 50 )
const material = new THREE.MeshStandardMaterial( { color: 0xcc0000 , wireframe: true } );
const torus = new THREE.Mesh( geometry, material );


const pointLight = new THREE.PointLight(0xff00ff)
const ambientLight = new THREE.AmbientLight(0xffffff);

pointLight.position.set( 15, 15, 15 )
ambientLight.position.set( 25, 25, 25 )

const lightHelper = new THREE.PointLightHelper(pointLight)
const gridHelper = new THREE.GridHelper( 200, 100 );


function addStar() {
  const geometry = new THREE.SphereGeometry( 0.25, 24, 24 );
  const material = new THREE.MeshStandardMaterial( { color: 0xffffff } )
  const star = new THREE.Mesh( geometry, material );
  const [x, y, z] = Array(3).fill().map(() => THREE.MathUtils.randFloatSpread( 100 ) );
  star.position.set( x, y, z );

  scene.add(star)
}
Array(200).fill().forEach(addStar)

scene.add( torus, pointLight, ambientLight, lightHelper, gridHelper)

const controls = new OrbitControls(camera, renderer.domElement);


function animate() {
  requestAnimationFrame( animate );

  torus.rotation.x +=0.002;
  torus.rotation.y -=0.002;
  torus.rotation.z -=0.002;
  
  controls.update();
  renderer.render( scene, camera );
}

animate()
